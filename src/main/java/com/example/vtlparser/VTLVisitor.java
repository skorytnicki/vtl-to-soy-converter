package com.example.vtlparser;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.context.InternalContextAdapter;
import org.apache.velocity.context.InternalContextAdapterImpl;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.*;

import java.io.*;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class NodeData extends Object {
    private String indent = "";
    NodeData (String indent)  {
        this.indent = indent;
    }
    public String getIndent() {
        return indent;
    }
}

public class VTLVisitor implements ParserVisitor {
    /**
     * soy puts expressions in curly braces, like <option>{$value}<option/>
     * This does not apply to foreach, if, or template calls, consider: {if $value}, {foreach $value in $values}
     */
    private final Integer[] OMIT_BRACES_TYPES = {
            0, // process
            10, // directive
            16, // reference
            23, // Set
            25, // truthy
            27, // OR
            28, // foreach
            29, // EQ
            30, // !=
            31, // LT
            32, // GT
            33, // LTE
            34, // GTE
            40, // not
    };

    public void print(String string) {
        System.out.print(string);
    }

    public void println(String string) {
        print(string + "\n");
    }

    public void visitChildren(SimpleNode node, NodeData data) {
        node.childrenAccept(this, data);
    }

    @Override
    public Object visit(ASTStop simpleNode, Object data) {
        return null;
    }

    @Override
    public Object visit(SimpleNode simpleNode, Object data) {
        return null;
    }

    @Override
    public Object visit(ASTprocess node, Object data) {
        visitChildren(node, new NodeData(""));
        return null;
    }

    @Override
    public Object visit(ASTText node, Object data) {
        String value = NodeUtils.tokenLiteral(node.getFirstToken());
        print(value);
        return null;
    }

    @Override
    public Object visit(ASTEscapedDirective node, Object data) {
        visitChildren(node, (NodeData) data);
        return null;
    }

    @Override
    public Object visit(ASTEscape node, Object data) {
        visitChildren(node, (NodeData) data);
        return null;
    }

    @Override
    public Object visit(ASTComment node, Object data) {
        return null;
    }

    @Override
    public Object visit(ASTFloatingPointLiteral node, Object data) {
        print(node.literal());
        visitChildren(node, (NodeData) data);
        return null;
    }

    @Override
    public Object visit(ASTIntegerLiteral node, Object data) {
        print(node.literal());
        return null;
    }

    @Override
    public Object visit(ASTStringLiteral node, Object data) {
        print(node.literal());
        visitChildren(node, (NodeData) data);
        return null;
    }

    @Override
    public Object visit(ASTIdentifier node, Object data) {
        print("." + node.literal());
        visitChildren(node, (NodeData) data);
        return null;
    }

    @Override
    public Object visit(ASTWord node, Object data) {
        if (node.literal().equals("in")) {
            print(" in ");
        } else {
            print(node.literal());
        }
        return null;
    }

    @Override
    public Object visit(ASTDirective node, Object data) {
        String value = node.getDirectiveName();
        if (value.equals("foreach")) {
            node.jjtAccept(new VTLForeachVisitor(), data);
        } else if (value.equals("macro")) {
            node.jjtAccept(new VTLMacroVisitor(), data);
        } else {
            node.jjtAccept(new VTLCallVisitor(), data);
        }
        return null;
    }

    @Override
    public Object visit(ASTBlock node, Object data) {
        visitChildren(node, (NodeData) data);
        return null;
    }


    @Override
    public Object visit(ASTMap node, Object data) {
        visitChildren(node, (NodeData) data);
        return null;
    }

    @Override
    public Object visit(ASTObjectArray node, Object data) {
        visitChildren(node, (NodeData) data);
        return null;
    }

    @Override
    public Object visit(ASTIntegerRange node, Object data) {
        visitChildren(node, (NodeData) data);
        return null;
    }

    @Override
    public Object visit(ASTMethod node, Object data) {
        print("." + node.literal());
        return null;
    }

    @Override
    public Object visit(ASTReference node, Object data) {
        String value = node.getRootString();

        if (!Arrays.asList(OMIT_BRACES_TYPES).contains(node.jjtGetParent().getType())) {
            print("{");
        }
        if (node.literal().contains("$!") || node.literal().contains("!$")) {
            print("!");
        }
        print("$" + value);
        visitChildren(node, (NodeData) data);

        if (!Arrays.asList(OMIT_BRACES_TYPES).contains(node.jjtGetParent().getType())) {
            print("}");
        } else {
            print(" ");
        }
        return null;
    }

    @Override
    public Object visit(ASTTrue node, Object data) {
        print("true");
        return null;
    }

    @Override
    public Object visit(ASTFalse node, Object data) {
        print("false");
        return null;
    }

    @Override
    public Object visit(ASTIfStatement node, Object data) {
        node.jjtAccept(new VTLIfVisitor(), data);
        return null;
    }

    @Override
    public Object visit(ASTElseStatement node, Object data) {
        return null;
    }

    @Override
    public Object visit(ASTElseIfStatement node, Object data) {
        return null;
    }

    @Override
    public Object visit(ASTSetDirective node, Object data) {
        final String value = node.literal();
        String directiveContent = "";

        String regexString = Pattern.quote("(") + "(.*?)$";
        Pattern pattern = Pattern.compile(regexString);
        Matcher matcher = pattern.matcher(value);

        while (matcher.find()) {
            directiveContent = matcher.group(1); // Since (.*?) is capturing group 1
        }
        directiveContent = directiveContent.replace(" =", ":");
        directiveContent = directiveContent.substring(0, directiveContent.length() - 1);
        directiveContent = directiveContent.replaceAll("\"", "'");
        directiveContent = directiveContent.replace("' ${", "$");
        directiveContent = directiveContent.replace("'${", "$");
        directiveContent = directiveContent.replace("}'", "");

        println("{let " + directiveContent + " /}");
        return null;
    }

    @Override
    public Object visit(ASTExpression node, Object data) {
        String name = node.getClass().getName().replace("org.apache.velocity.runtime.parser.node.", "");
        String value = ""; // todo
        String indent = ((NodeData) data).getIndent();
        Boolean hasChildren = node.jjtGetNumChildren() > 0;

        visitChildren(node, (NodeData) data);
        return null;
    }

    @Override
    public Object visit(ASTAssignment node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" = ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTOrNode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" or ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTAndNode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" and ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTEQNode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" == ");
        node.jjtGetChild(1).jjtAccept(this, data);

        return null;
    }

    @Override
    public Object visit(ASTNENode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" !== ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTLTNode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" < ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTGTNode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" > ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTLENode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" <= ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTGENode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" >= ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTAddNode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" + ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTSubtractNode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" - ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTMulNode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" * ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTDivNode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" / ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTModNode node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        print(" % ");
        node.jjtGetChild(1).jjtAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTNotNode node, Object data) {
        print(" not ");
        visitChildren(node, (NodeData) data);
        return null;
    }

    public void generate(SimpleNode node) {
        // recursively traverse the AST
        println("{namespace TODO." + node.getTemplateName().replace("src/source/","").replace(".vm", "") + "}");
        node.jjtAccept(this, null);
    }
}

class Traverser {
    private static final String TEMPLATE_FILE_PATH = "src/source/source.vm";

    public static void main(String[] args) throws Exception {
        VTLVisitor vtlVisitor = new VTLVisitor();
        SimpleNode ast = getInitedAST();
        vtlVisitor.generate(ast);
    }

    private static Reader getTemplateReader() throws Exception {
        return new BufferedReader(new InputStreamReader(new FileInputStream(TEMPLATE_FILE_PATH)));
    }

    private static SimpleNode getInitedAST() throws Exception {
        SimpleNode ast = RuntimeSingleton.parse(getTemplateReader(), TEMPLATE_FILE_PATH);
        InternalContextAdapter context = new InternalContextAdapterImpl(new VelocityContext());
        context.pushCurrentTemplateName(TEMPLATE_FILE_PATH);
        ast.init(context, RuntimeSingleton.getRuntimeServices());
        return ast;
    }
}