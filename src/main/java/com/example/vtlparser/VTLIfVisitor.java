package com.example.vtlparser;
import org.apache.velocity.runtime.parser.node.*;

public class VTLIfVisitor extends VTLVisitor implements ParserVisitor {

    /**
     * In if statements, first ASTExpression is a condition;
     * This function adds a curly brace at the end of the condition.
     */
    @Override
    public Object visit(ASTExpression node, Object data) {
        visitChildren(node, (NodeData) data);

        if (node.literal().equals(node.jjtGetParent().jjtGetChild(0).literal())) {
            print("}");
            println("");
        }
        return null;
    }

    @Override
    public Object visit(ASTIfStatement node, Object data) {
        print("{if "); // closing brace is added by the ASTExpression visitor
        visitChildren(node, (NodeData) data);
        println("{/if}");
        return null;
    }

    @Override
    public Object visit(ASTElseStatement node, Object data) {
        println("{else}");
        visitChildren(node, (NodeData) data);
        return null;
    }

    @Override
    public Object visit(ASTElseIfStatement node, Object data) {
        print("{elseif ");
        visitChildren(node, (NodeData) data);
        return null;
    }
}
