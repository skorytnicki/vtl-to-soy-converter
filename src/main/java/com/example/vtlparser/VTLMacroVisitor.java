package com.example.vtlparser;

import org.apache.velocity.runtime.parser.node.ASTDirective;
import org.apache.velocity.runtime.parser.node.ASTReference;
import org.apache.velocity.runtime.parser.node.ASTWord;

public class VTLMacroVisitor extends VTLVisitor {
    public Object visit(ASTDirective node, Object data) {
        if (node.getDirectiveName().equals("macro")) {
            println("/**");
            for(int i = 0; i < node.jjtGetNumChildren(); i++) {
                if (node.jjtGetChild(i).getType() == 16) {
                    final ASTReference ref = (ASTReference) node.jjtGetChild(i);
                    final String refName = ref.getRootString();
                    println(" * @param " + refName);
                }
            }
            println(" */");

            println("{template ." + node.jjtGetChild(0).literal() + "}");
            visitChildren(node, (NodeData) data);
            println("{/template}");
        } else {
            super.visit(node, data);
        }
        return null;
    }

    @Override
    public Object visit(ASTWord node, Object data) {
        if (node.jjtGetParent().getType() == 10 && node.jjtGetParent().jjtGetChild(0).literal().equals(node.literal())) {
            return null;
        } else {
            super.visit(node, data);
        }
        return null;
    }

    @Override
    public Object visit(ASTReference node, Object data) {
        if (node.jjtGetParent().getType() != 10) {
            super.visit(node, data);
        }
        return null;
    }
}
