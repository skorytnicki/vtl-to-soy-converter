package com.example.vtlparser;

import org.apache.velocity.runtime.parser.node.ASTDirective;
import org.apache.velocity.runtime.parser.node.ASTFalse;
import org.apache.velocity.runtime.parser.node.ASTReference;
import org.apache.velocity.runtime.parser.node.ASTTrue;

public class VTLCallVisitor extends VTLVisitor {
    public Object visit(ASTDirective node, Object data) {
        println("");
        println("{call ." + node.getDirectiveName() + "}");
        visitChildren(node, (NodeData) data);
        println("{/call}");
        return null;
    }

    @Override
    public Object visit(ASTReference node, Object data) {
        String value = node.getRootString();
        String method = "";
        if (node.jjtGetNumChildren() > 0) {
            method = node.jjtGetChild(0).literal();
        }
        println("{param someParam: $"+value+ (!method.equals("") ? "." + method : "") + " /}");
        return null;
    }
    @Override
    public Object visit(ASTTrue node, Object data) {
        println("{param someParam: true /}");
        return null;
    }
    @Override
    public Object visit(ASTFalse node, Object data) {
        println("{param someParam: false /}");
        return null;
    }
}
