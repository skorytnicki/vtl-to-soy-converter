package com.example.vtlparser;

import org.apache.velocity.runtime.parser.node.ASTDirective;
import org.apache.velocity.runtime.parser.node.ASTReference;

public class VTLForeachVisitor extends VTLVisitor {
    public Object visit(ASTDirective node, Object data) {
        String value = node.getDirectiveName();
        if (value.equals("foreach")) {
            print("{foreach ");
            visitChildren(node, (NodeData) data);
            println("{/foreach}");
        } else {
            super.visit(node, data);
        }

        return null;
    }

    @Override
    public Object visit(ASTReference node, Object data) {
        super.visit(node, data);
        if (node.jjtGetParent().jjtGetNumChildren() > 2) {
            if (node.literal().equals(node.jjtGetParent().jjtGetChild(2).literal())) {
                print("}");
                println("");
            }
        }
        return null;
    }
}
